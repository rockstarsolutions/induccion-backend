<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests\Task\StoreTask;
use App\Http\Requests\Task\UpdateTask;

class TasksController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$tasks = Task::oldest()->get();
		return response()->json($tasks);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  App\Http\Requests\Task\StoreTask  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreTask $request)
	{
		$task = Task::create([
			'description' => $request->description,
			'day' => $request->day
		]);
		return response()->json(Task::find($task->id), 201);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function show(Task $task)
	{
		return response()->json($task);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  App\Http\Requests\Task\UpdateTask  $request
	 * @param  \App\Task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateTask $request, Task $task)
	{
		if ($request->description) $task->description = $request->description;
		if ($request->day) $task->day = $request->day;
		if ($request->finished) $task->finished = $request->finished;
		$task->save();
		return response()->json($task);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Task $task)
	{
		$task->delete();
		return response()->json(['message' => 'Tarea con ID ' . $task->id . ' eliminada']);
	}
}
