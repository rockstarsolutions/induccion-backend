<?php

namespace App\Http\Requests\Task;

use App\Http\Requests\BaseRequest;

class StoreTask extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|min:1',
            'day' => 'required|date_format:Y-m-d'
        ];
    }
}
