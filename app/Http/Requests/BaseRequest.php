<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseRequest extends FormRequest
{
	protected function failedValidation(Validator $validator) {
		$errorsKeys = $validator->errors()->keys();
		foreach ($errorsKeys as $key) {
			$errors[] = ['field' => $key, 'message' => $validator->errors()->get($key)[0]];
		}
        throw new HttpResponseException(response()->json($errors, 400));
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
        	'day' => 'día',
        	'description' => 'descripción'
        ];
    }
}