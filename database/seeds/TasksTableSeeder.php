<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Task;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$today = Carbon::now();
        $tasks = [
	        [
				'description' => 'Lavar el auto',
				'day' => $today,
				'finished' => false
	        ],
			[
				'description' => 'Pagar las cuentas',
				'day' => $today->clone()->addDays(1),
				'finished' => false
	        ],
			[
				'description' => 'Hacer la tarea',
				'day' => $today->clone()->addDays(1),
				'finished' => false
	        ],
			[
				'description' => 'Lavar la ropa',
				'day' => $today->clone()->addDays(2),
				'finished' => false
	        ],
			[
				'description' => 'Llevar los niños a la playa',
				'day' => $today->clone()->addDays(3),
				'finished' => false
	        ],
			[
				'description' => 'Lavar los platos',
				'day' => $today->clone()->addDays(3),
				'finished' => false
	        ],
			[
				'description' => 'Comprar comestibles',
				'day' => $today->clone()->addDays(3),
				'finished' => false
	        ]
        ];
        foreach ($tasks as $task) {
	        Task::create($task);
        }
    }
}
